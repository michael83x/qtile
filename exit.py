#! /usr/bin/python

import gi
import os
import subprocess
from gi.repository import Gtk, Pango
gi.require_version("Gtk", "3.0")


class MyWindow(Gtk.Window):
    def __init__(self):
        super().__init__(title=" GoodBye Qtile ")

        self.set_border_width(10)
        self.set_default_size(800, 200)
        self.set_position(Gtk.WindowPosition.CENTER)
        self.set_resizable(False)

        self.box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.add(self.box)

        self.button1 = Gtk.Button()
        self.label1 = Gtk.Label("󰷛")
        self.label1.modify_font(Pango.FontDescription('32'))
        self.button1.add(self.label1)
        self.button1.connect("clicked", self.on_button1_clicked)
        self.box.pack_start(self.button1, True, True, 0)

        self.button2 = Gtk.Button()
        self.label2 = Gtk.Label("󰍃")
        self.label2.modify_font(Pango.FontDescription('32'))
        self.button2.add(self.label2)
        self.button2.connect("clicked", self.on_button2_clicked)
        self.box.pack_start(self.button2, True, True, 0)

        self.button3 = Gtk.Button()
        self.label3 = Gtk.Label("󰜉")
        self.label3.modify_font(Pango.FontDescription('32'))
        self.button3.add(self.label3)
        self.button3.connect("clicked", self.on_button3_clicked)
        self.box.pack_start(self.button3, True, True, 0)

        self.button4 = Gtk.Button()
        self.label4 = Gtk.Label('')
        self.label4.modify_font(Pango.FontDescription('32'))
        self.button4.add(self.label4)
        self.button4.connect("clicked", self.on_button4_clicked)
        self.box.pack_start(self.button4, True, True, 0)

    def on_button1_clicked(self, widget):
        subprocess.run(["i3lock", "-B", "sigma"])
        Gtk.main_quit()

    def on_button2_clicked(self, widget):
        os.system("loginctl terminate-user $USER")

    def on_button3_clicked(self, widget):
        subprocess.run(["reboot"])

    def on_button4_clicked(self, widget):
        command = "mpv --no-video ~/.config/qtile/preview_4.mp3 && shutdown now"
        subprocess.run(command, shell=True, check=True)


win = MyWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
