import os
import subprocess
from typing import List  # noqa: F401

from libqtile import bar, layout, widget, hook, qtile, extension
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from qtile_extras import widget
from qtile_extras.widget.decorations import PowerLineDecoration
import colors

mod = "mod4"
terminal = guess_terminal("kitty")
myfont = "JetBrainsMono Nerd Font"

keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
    Key([mod], "p", lazy.run_extension(extension.DmenuRun(
        dmenu_command="./dmenu_run_history",
        font='SourceCodePro',
        fontsize=15,
        background=colors.colors[colors.num]["bg"],
        foreground=colors.colors[colors.num]["fg"],
        selected_background=colors.colors[colors.num]["2"],
        selected_foreground=colors.colors[colors.num]["0"],
        )),
        desc="program launcher"),
    Key([mod], "f", lazy.spawn("pcmanfm"), desc="file manager"),
    Key([mod], "b", lazy.spawn("firefox"), desc="web browser"),
    Key([mod], "x", lazy.spawn("./.config/qtile/exit.py"), desc="exit script"),
    Key([mod], "e", lazy.spawn("emacsclient -c -a 'emacs'"), desc="text editor"),
    # Key([mod], "c", lazy.spawn(".config/qtile/themes"), desc="qtile themes selector"),
    Key([], "Print", lazy.spawn("scrot"), desc="print screen"),
    Key([mod], "Left", lazy.screen.next_group(skip_empty=True), desc="move to next workspace"),
    Key([mod], "Right", lazy.screen.prev_group(skip_empty=True), desc="move to previous workspace"),
]

groups = [
        Group('1', label='  ', matches=[Match(wm_class=["kitty"])]),
        Group('2', label=' 󰖟 ', matches=[Match(wm_class=["firefox"])]),
        Group('3', label=' 󰈤 ', matches=[Match(wm_class=["emacs"])]),
        Group('4', label='  ', matches=[Match(wm_class=["pcmanfm", "virt-manager"])]),
        Group('5', label='  ', matches=[Match(wm_class=["pamac-manager", "lxappearance"])]),
        ]

for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + letter of group = switch to & move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(i.name),
            ),
            # Or, use below if you prefer not to switch to that group.
            # # mod1 + shift + letter of group = move focused window to group
            # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            #     desc="move focused window to group {}".format(i.name)),
        ]
    )

layouts = [
    layout.Columns(border_focus_stack=["#d75f5f", "#8f3d3d"], border_width=2, margin=5),
    layout.Floating(border_width=2),
    # Try more layouts by unleashing below layouts.
    # layout.Max(),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadTall(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font=myfont,
    fontsize=15,
    padding=3,
    background=colors.colors[colors.num]['bg'],
    foreground=colors.colors[colors.num]['fg'],
)
extension_defaults = widget_defaults.copy()


def date_notification():
    os.system('dunstify "Date" "$(date)" -i /usr/share/icons/Nordzy/mimes/16/calendar.svg')


powerline1 = {
        "decorations": [
                    PowerLineDecoration()
                ]
}

powerline2 = {
        "decorations": [
                    PowerLineDecoration(path="arrow_right")
                ]
}

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.Wallpaper(
                    fontsize=20,
                    directory="/usr/share/backgrounds/nordic-wallpapers/",
                    label="  ",
                    random_selection=True,
                    foreground=colors.colors[colors.num]['4'],
                    background=colors.colors[colors.num]['bg'],
                    **powerline1,
                ),
                widget.GroupBox(
                    font=myfont,
                    borderwidth=2,
                    highlight_method='line',
                    fontsize=18,
                    disable_drag=True,
                    active=colors.colors[colors.num]['bg'],
                    inactive=colors.colors[colors.num]['7'],
                    highlight_color=colors.colors[colors.num]['2'],
                    background=colors.colors[colors.num]['2'],
                    **powerline1,
                ),
                widget.Spacer(length=400),
                widget.WindowName(
                    foreground=colors.colors[colors.num]['3'],
                    # **powerline1,
                ),
                widget.Chord(
                    chords_colors={
                        "launch": ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.Spacer(
                    length=1,
                    **powerline2,
                ),
                widget.Prompt(
                    prompt=' </> ',
                    foreground=colors.colors[colors.num]['1'],
                    background=colors.colors[colors.num]['bg'],
                    **powerline2,
                ),
                widget.Wttr(
                    location={'New Delhi': 'Home'},
                    font=myfont,
                    background=colors.colors[colors.num]['14'],
                    foreground=colors.colors[colors.num]['bg'],
                    format='%l %m %t(%f)',
                    **powerline2,
                ),
                widget.Clock(
                    font=myfont,
                    fontsize=15,
                    format="%I:%M %p ",
                    foreground=colors.colors[colors.num]['0'],
                    background=colors.colors[colors.num]['3'],
                    mouse_callbacks={'Button1': lambda: qtile.cmd_spawn(date_notification())},
                    **powerline2,
                ),
                widget.Systray(
                    background=colors.colors[colors.num]['bg'],
                ),
                widget.CurrentLayoutIcon(
                    scale=0.5
                ),
            ],
            28,
            margin=5,
            opacity=0.9,
            # border_width=[1, 1, 1, 1],  # Draw top and bottom borders
            # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # Borders are magenta
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="lxappearance"),  # gitk
        Match(wm_class="Torrential"),
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.run([home])


# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

wl_input_rules = None

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "Qtile"
