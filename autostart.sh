#!/bin/sh

lxsession &
nm-applet &
volumeicon &
cbatticon &
picom --experimental-backends &
xautolock -detectsleep -time 5 -locker "i3lock -B sigma" &
mpv --no-video ~/.config/qtile/Computer_Magic.mp3 &
